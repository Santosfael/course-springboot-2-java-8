package br.edu.iftm.course.entities.enuns;

public enum TypeClient {
	PESSOAFISICA(0, "Pessoa Física"),
	PESSOAJURIDICA(1, "Pessoa Jurídica");
	
	private int code;
	private String description;
	
	private TypeClient(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
	
	public static TypeClient valueOf(int code) {
		for(TypeClient value : TypeClient.values()) {
			if(value.getCode() == code) {
				return value;
			}
		}
		throw new IllegalArgumentException("Invalid TypeClient code");
	}
	/*public static TypeClient toEnum(Integer code) {
		if(code == null) {
			return null;
		}
		for(TypeClient x: TypeClient.values()) {
			if(code.equals(x.getCode())) {
				return x;
			}
		}
		throw new IllegalArgumentException("Id inválido: "+code);
	}
	
	public static TypeClient toEnum(String code) {
		if(code == null) {
			return null;
		}
		for(TypeClient x: TypeClient.values()) {
			if(code.equals(x.getDescription())) {
				return x;
			}
		}
		throw new IllegalArgumentException("Id inválido: "+code);
	}*/
}
