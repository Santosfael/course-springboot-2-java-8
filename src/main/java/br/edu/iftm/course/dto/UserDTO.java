package br.edu.iftm.course.dto;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import br.edu.iftm.course.entities.User;
import br.edu.iftm.course.entities.enuns.TypeClient;
import br.edu.iftm.course.services.validation.UserUpdateValid;

@UserUpdateValid
public class UserDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	@NotEmpty(message = "Can't be empty")
	@Length(min = 5, max = 80, message = "length must be between 5 and 80")
	private String name;
	
	@NotEmpty(message = "Can't be empty")
	@Email(message = "invalid e-mail")
	private String email;
	
	@NotEmpty(message = "Can't be empty")
	private String cpfOuCnpj;
	
	//@NotEmpty(message = "Can't be empty")
	private TypeClient type;
	
	//@NotEmpty(message = "Can't be empty")
	//@Length(min = 8, max = 20, message = "length must be between 8 and 20")
	private Set<String> phone;
	
	public UserDTO() {}

	public UserDTO(Long id, String name, String email, String cpfOucCnpj, TypeClient type, Set<String> phone) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.cpfOuCnpj = cpfOucCnpj;
		this.type = type;
		this.phone = phone;
	}
	
	public UserDTO(User entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.email = entity.getEmail();
		this.cpfOuCnpj = entity.getCpfOuCnpj();
		this.type = entity.getType();
		this.phone = entity.getPhones();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getPhone() {
		return phone;
	}

	public void setPhone(Set<String> phone) {
		this.phone = phone;
	}
	
	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public TypeClient getType() {
		return type;
	}

	public void setType(TypeClient type) {
		this.type = type;
	}

	public User toEntity() {
		//return new User(id, name, email, phone, null);
		return new User(id, name, email, cpfOuCnpj, type, null);
	}
}
