package br.edu.iftm.course.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.iftm.course.entities.State;

public interface StateRepository extends JpaRepository<State, Long>{
	 

}
