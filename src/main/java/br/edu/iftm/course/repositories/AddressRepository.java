package br.edu.iftm.course.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.iftm.course.entities.Address;

public interface AddressRepository extends JpaRepository<Address, Long>{
	 

}
