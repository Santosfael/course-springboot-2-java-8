package br.edu.iftm.course.config;

import java.time.Instant;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.edu.iftm.course.entities.Address;
import br.edu.iftm.course.entities.Category;
import br.edu.iftm.course.entities.City;
import br.edu.iftm.course.entities.Order;
import br.edu.iftm.course.entities.OrderItem;
import br.edu.iftm.course.entities.Payment;
import br.edu.iftm.course.entities.Product;
import br.edu.iftm.course.entities.Role;
import br.edu.iftm.course.entities.State;
import br.edu.iftm.course.entities.User;
import br.edu.iftm.course.entities.enuns.OrderStatus;
import br.edu.iftm.course.entities.enuns.TypeClient;
import br.edu.iftm.course.repositories.AddressRepository;
import br.edu.iftm.course.repositories.CategoryRepository;
import br.edu.iftm.course.repositories.CityRepository;
import br.edu.iftm.course.repositories.OrderItemRepository;
import br.edu.iftm.course.repositories.OrderRepository;
import br.edu.iftm.course.repositories.ProductRepository;
import br.edu.iftm.course.repositories.RoleRepository;
import br.edu.iftm.course.repositories.StateRepository;
import br.edu.iftm.course.repositories.UserRepository;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner{
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderItemRepository orderItemRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private AddressRepository addressRepository;

	@Override
	public void run(String... args) throws Exception {
		
		State est1 = new State(null, "Minas Gerais");
		State est2 = new State(null, "São Paulo");
		
		City c1 = new City(null, "Uberlândia", est1);
		City c2 = new City(null, "São Paulo", est2);
		City c3 = new City(null, "Campinas", est2);
		
		est1.getCities().addAll(Arrays.asList(c1));
		est2.getCities().addAll(Arrays.asList(c2, c3));
		
		stateRepository.saveAll(Arrays.asList(est1, est2));
		cityRepository.saveAll(Arrays.asList(c1, c2, c3));
		
		Category cat1 = new Category(null, "Eletronics");
		Category cat2 = new Category(null, "Books");
		Category cat3 = new Category(null, "Computers");
		
		Product p1 = new Product(null,"The Lord of the Rings", "Lorem ipsum dolor sit amet, ", 90.5, "");
		Product p2 = new Product(null,"Smart TV", "Nulla eu imperdient purus. Maecenas ante.", 2190.0, "");
		Product p3 = new Product(null,"Macbook Pro", "Nam eleifend maximus tortor, at mollis.", 1250.0, "");
		Product p4 = new Product(null,"PC Gamer", "Donecaliquet odio ac rhoncus cursus.", 1200.00, "");
		Product p5 = new Product(null,"Rails for Dummies"," Cras frigilla convallis sem vel faucibus", 100.99, "");
		
		categoryRepository.saveAll(Arrays.asList(cat1, cat2, cat3));
		productRepository.saveAll(Arrays.asList(p1, p2, p3, p4, p5));
		
		p1.getCategories().add(cat2);
		p2.getCategories().add(cat1);
		p2.getCategories().add(cat3);
		p3.getCategories().add(cat3);
		p4.getCategories().add(cat3);
		p5.getCategories().add(cat2);
		
		productRepository.saveAll(Arrays.asList(p1, p2, p3, p4, p5));
		
		
		User u1 = new User(null, "Maria Brown", "maria@gmail.com", "12345678910", TypeClient.PESSOAFISICA, passwordEncoder.encode("123456"));
		User u2 = new User(null, "Alex Green", "alex@gmail.com", "12345678920",TypeClient.PESSOAFISICA, passwordEncoder.encode("123456"));
		User u3 = new User(null, "Jorge Green", "Jorge@gmail.com", "12345678920",TypeClient.PESSOAFISICA, passwordEncoder.encode("123456"));
		
		u1.getPhones().addAll(Arrays.asList("992269887","997985171"));
		
		Address e1 = new Address(null, "Rua FLores", "300", "Apto 303", "Jardim", "38220834", u1, c1);
		Address e2 = new Address(null, "Avenida Matos", "105", "Sala 800", "Centro", "38777012", u1, c2);
		
		u1.getAdresses().addAll(Arrays.asList(e1, e2));
		
		userRepository.saveAll(Arrays.asList(u1, u2, u3));
		
		addressRepository.saveAll(Arrays.asList(e1, e2));
		
		Role r1 = new Role(null, "ROLE_CLIENT");
		Role r2 = new Role(null, "ROLE_ADMIN");
		
		roleRepository.saveAll(Arrays.asList(r1, r2));
		
		u1.getRoles().add(r1);
		u2.getRoles().add(r1);
		u2.getRoles().add(r2);
		
		userRepository.saveAll(Arrays.asList(u1, u2));
		
		Order o1 = new Order(null, Instant.parse("2019-06-20T19:53:07Z"),OrderStatus.PAID, u1, e1);
		Order o2 = new Order(null, Instant.parse("2019-07-21T03:42:10Z"), OrderStatus.WAITING_PAYMENT, u2, e2);
		Order o3 = new Order(null, Instant.parse("2019-06-22T15:21:22Z"), OrderStatus.WAITING_PAYMENT, u1, e2);
		
		orderRepository.saveAll(Arrays.asList(o1, o2, o3));
		
		
		 OrderItem oi1 = new OrderItem(o1, p1, 2, p1.getPrice()); OrderItem oi2 = new
		 OrderItem(o1, p3, 2, p3.getPrice()); OrderItem oi3 = new OrderItem(o2, p3, 2,
		 p3.getPrice()); OrderItem oi4 = new OrderItem(o3, p5, 2, p5.getPrice());
		 
		 orderItemRepository.saveAll(Arrays.asList(oi1, oi2, oi3, oi4));
		 
		 Payment pay1 = new Payment(null, Instant.parse("2019-06-20T21:53:07Z"),o1);
		 o1.setPayment(pay1);
		 
		 orderRepository.save(o1);
		 
		
	}
	
	
}
